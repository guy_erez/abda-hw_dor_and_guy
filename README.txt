We used collab this time!
 
The link to our collab directory: 
https://drive.google.com/drive/folders/1zIoF90AHSnD86IfG-Oegl1YjiODu-dRN?usp=sharing

The iris collab notebook link:
https://colab.research.google.com/drive/1pkLUuujIyjMBYI4UP8pVdsqQGja20lw-?usp=sharing

The covid19 collab notebook link:
https://colab.research.google.com/drive/1t8bgf7L0IISH2cp3kBTn8sVns-_6bZ9y?usp=sharing

We also added the Jupyter notebooks to the repo.

Thanks, 
Guy and Dor 