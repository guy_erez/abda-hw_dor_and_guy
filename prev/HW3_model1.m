results = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results.csv');
companies = ["Uber","Lyft","Bolt","Yango","Gett"];
 


n = [26 17 11 5 3]; 
avg_wait_time_data = [7.8 14.4 12.0 20.4 6.5];

m = length(results); 
beta = round(exp(results(:,1)),0);
lambdas = round(exp(results(:,2:6)),4);
avg_wait_time = 1./lambdas;


[beta_count,beta_vals] = hist(beta,unique(beta)); 
E_lambda_given_beta = zeros(length(beta_vals),5); 

for i = 1:length(beta_vals)
    E_lambda_given_beta(i,:) = sum(lambdas(beta==beta_vals(i),:))/beta_count(i); 
end

figure
title('Sensetivity Check')
subplot(2,1,1)
for i = 1:5
    plot(beta_vals,1./E_lambda_given_beta(:,i),'o-')
    hold on 
end
legend(companies);
ylabel('E(1/\lambda | \beta , y)','FontSize',16,'FontWeight','bold')
xlim([2 30])

subplot(2,1,2)
plot(beta_vals,beta_count,'o-')
xlabel('\beta','FontSize',16,'FontWeight','bold')
ylabel('P(\beta|y)','FontSize',16,'FontWeight','bold')
xlim([2 30])

means = mean(avg_wait_time,1); 
confidence_interval = sort(avg_wait_time,1);
confidence_interval = [confidence_interval(m*0.05,:); confidence_interval(m*0.95,:)];



%%
y_rep = gamrnd(ones(m,1)*n,1./(n.*lambdas)); 
y_rep_means = mean(y_rep,2);
y_rep_std = std(y_rep,0,2);
y_rep_max = max(y_rep,[],2);
y_rep_min= min(y_rep,[],2);

figure
subplot(2,2,1)
test_quantity_hist(y_rep_means,mean(avg_wait_time_data),'mean(y)',0.3,0.55);
subplot(2,2,2)
test_quantity_hist(y_rep_std,std(avg_wait_time_data),'std(y)',0.75,0.55);
subplot(2,2,3)
test_quantity_hist(y_rep_max,max(avg_wait_time_data),'max(y)',0.3,0.09);
subplot(2,2,4)
test_quantity_hist(y_rep_min,min(avg_wait_time_data),'min(y)',0.75,0.09);

function [x] = test_quantity_hist(y_rep_vals, data_val,title,plot_pos_x,plot_pos_y)
    histogram(y_rep_vals,80,'Normalization','probability');
    line([data_val, data_val], [0, 0.25], 'Color', 'r', 'LineWidth', 0.9);
    dim = [plot_pos_x plot_pos_y 0.3 0.3];
    p_val_str = {['p value : ' num2str(length(y_rep_vals(y_rep_vals>data_val))/length(y_rep_vals))]};
    annotation('textbox',dim,'String',p_val_str,'FitBoxToText','on','FontSize',16,'FontWeight','bold');
    xlabel(['T(y) = ',title],'FontSize',16,'FontWeight','bold')
    x = 1;
end

