package main

import (
	. "bitbucket.org/guy_erez/abda-hw_dor_and_guy/model/ad"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	"flag"
	"fmt"
	"math"
	"math/rand"
	"os"
	"strings"
	"time"
	"io"
	//"gonum.org/v1/plot"
	//"gonum.org/v1/plot/plotter"
	//"gonum.org/v1/plot/plotutil"
	//"gonum.org/v1/plot/vg"

)

// Command line arguments

var (
	RATE      = 0.01
	GAMMA     = 0.9
	NITER     = 10000
	OPTIMIZER = "Adam"
	HMC_NSTEPS = 5
	HMC_STEP   = 0.1
)

func init() {
	flag.Usage = func() {
		fmt.Printf(`Eight school example. Usage:
		schools [OPTIONS]` + "\n")
		flag.PrintDefaults()
	}
	flag.Float64Var(&RATE, "rate", RATE, "learning rate")
	flag.Float64Var(&GAMMA, "gamma", GAMMA, "momentum factor")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.StringVar(&OPTIMIZER, "optimizer", OPTIMIZER,"optimizer (Gradient, Momentum or Adam)")
	flag.IntVar(&HMC_NSTEPS, "hmc_nsteps", HMC_NSTEPS, "number of leapfrog steps")
	flag.Float64Var(&HMC_STEP, "hmc_step", HMC_STEP, "leapfrog step size")
	rand.Seed(time.Now().UnixNano())
}

func main() {
	flag.Parse()

	if flag.NArg() > 0 {
		fmt.Printf("unexpected positional arguments: %v",
			flag.Args())
		os.Exit(1)
	}

	// Define the problem
	m := &Model{
		J:     4,
		Y:     []float64{7.8, 14.4, 12.0, 20.4, 6.5},
		N:	   []float64{26,17,11,5,3},
	}

	x := make([]float64, 1 + m.J)
	fmt.Printf("initial values: \n")
	// Set a starting point
	for i := 0; i != len(x); i++ {
		x[i] = rand.NormFloat64()
		fmt.Printf("%f ,",x[i])
	}

	// Compute log-likelihood of the starting point,
	// for comparison.
	ll0 := m.Observe(x)
	model.DropGradient(m)

	// Create and run the optimizer
	var opt infer.Grad
	switch optimizer := strings.ToLower(OPTIMIZER); optimizer {
	case "gradient", "momentum":
		opt = &infer.Momentum{
			Rate:  RATE,
			Decay: math.Pow(0.1, 1/float64(NITER)),
		}
		if optimizer == "momentum" {
			opt.(*infer.Momentum).Gamma = GAMMA
		}
	case "adam":
		opt = &infer.Adam{Rate: RATE}
	default:
		fmt.Printf("unknown optimizer: %q", OPTIMIZER)
		os.Exit(1)
	}
	for iter := 0; iter != NITER; iter++ {
		opt.Step(m, x)
	}

	beta := math.Exp(x[0])
	lambda := x[1:]
	for i := range lambda {
		lambda[i] = math.Exp(lambda[i])
	}

	fmt.Printf("Finally:\n\tbeta=%.4g\n\tlambda=",beta)
	for _, lambda := range lambda {
		fmt.Printf("%.4g ", lambda)
	}
	fmt.Printf("\n\t1/lambda(Y) = ")
	for i, lambda := range lambda {
		fmt.Printf("%.4g(%.4g) ", 1/lambda,m.Y[i])
	}
	fmt.Printf("\n")
	ll := m.Observe(x)
	model.DropGradient(m)
	fmt.Printf("Log-likelihood: %.4g ⇒ %.4g\n", ll0, ll)


/////////////////////////////////////////////////////////
	s :=  ""	

	// Now let's infer the posterior with HMC.
	hmc := &infer.HMC{
		L:   HMC_NSTEPS,
		Eps: HMC_STEP / math.Sqrt(float64(len(m.Y))),
	}
	samples := make(chan []float64)
	hmc.Sample(m, x, samples)
	

	// Burn
	for i := 0; i != NITER; i++ {
		<-samples
	}

	// Collect after burn-in

	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		for i:= range x{
			s +=  fmt.Sprintf("%f ", x[i])
			s += ","
		}
		s += "\n"
	}
	hmc.Stop()

	// export samples 
	file, err := os.Create("results2.csv")
    defer file.Close()
	
	_, err = io.WriteString(file, s)

	if(err != nil){
		panic(err)
	}

}

