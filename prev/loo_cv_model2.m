alpha = 1.1; 
y = [7.8, 14.4, 12.0, 20.4, 6.5];
n = [26 17 11 5 3]; 
results1 = csvread('C:\Users\guy erez\Documents\ABDA\abda-course\examples\rides\results1.csv');
results2 = csvread('C:\Users\guy erez\Documents\ABDA\abda-course\examples\rides\results2.csv');
results3 = csvread('C:\Users\guy erez\Documents\ABDA\abda-course\examples\rides\results3.csv');
results4 = csvread('C:\Users\guy erez\Documents\ABDA\abda-course\examples\rides\results4.csv');
results5 = csvread('C:\Users\guy erez\Documents\ABDA\abda-course\examples\rides\results5.csv');
companies = ["Uber","Lyft","Bolt","Yango","Gett"];
S = size(results1,1);
mus = [results1(:,1),results2(:,1),results3(:,1),results4(:,1),results5(:,1)];
taus = [results1(:,2),results2(:,2),results3(:,2),results4(:,2),results5(:,2)];
etas = normrnd(mus,taus); 
%y~Gamma(n_j , n_j / eta_j) :
pdf_y_given_mu_tau = gampdf(ones(size(etas)).*y , ones(size(etas)).*n, 1./(n./etas));
pdf_y_given_mu_tau = nansum(pdf_y_given_mu_tau)/S;
log_pdf_y_given_mu_tau = log(pdf_y_given_mu_tau); 
lppd_loo_cv = sum(log_pdf_y_given_mu_tau); 
