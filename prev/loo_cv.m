alpha = 1.1; 
y = [7.8, 14.4, 12.0, 20.4, 6.5];
n = [26 17 11 5 3]; 
results1 = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results_cv1.csv');
results2 = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results_cv2.csv');
results3 = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results_cv3.csv');
results4 = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results_cv4.csv');
results5 = csvread('C:\Users\guy erez\Documents\ABDA\abda-hw_dor_and_guy\results_cv5.csv');
companies = ["Uber","Lyft","Bolt","Yango","Gett"];
S = size(results1,1);
betas = [results1(:,1),results2(:,1),results3(:,1),results4(:,1),results5(:,1)]; 
lambdas = gamrnd(alpha.*ones(size(betas)),betas); 
%y~Gamma(n_j , n_j * lambda_j) :
pdf_y_given_beta = gampdf(ones(size(betas)).*y , ones(size(betas)).*n, 1./(lambdas.*n));
pdf_y_given_beta = nansum(pdf_y_given_beta)/S;
log_pdf_y_given_beta = log(pdf_y_given_beta); 
lppd_loo_cv = sum(log_pdf_y_given_beta); 
