
package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"math"
)
// the model is : 
// y_ij ~ Gamma(n_j , n_j * lambda_j)    - y_ij = is the i avarge of waiting time in company j (at this ass i=1)   
// lambda_j ~ Gamma(alpha , beta)    - caus lambda > 0 
// log(beta) ~ Normal(0,5)			 - caus beta > 0
// alpha ~ consatnt 


type Model struct {
	J int       // number of ride-hailing companies
	Y []float64 // Average waiting time (minutes) of each company
	N []float64	// number of rides of each company	 (float64 cause of error: "cannot use m.N[j] (variable of type int) as float64 value in argument to Gamma.Logp")		
}
// x = [log(beta) , log(lambda_i) i = 1,...,5]
func (m *Model) Observe(x []float64) float64 {
	//  There are m.J + 1 parameters:
	// log(beta), log(lambda[J])
	beta := 1.1
	alpha := math.Exp(x[0])
	lambda := make([]float64, len(x)-1)
	for i := range lambda{
		lambda[i] = math.Exp(x[i+1])
	}

	prior_log_beta := Normal.Logp(0,5,x[0])   				//uninformative prior on log(beta)
	prior_lambda := Gamma.Logps(alpha,beta,lambda...)		
	
	y_given_lambda := 0.0
	for i:=range lambda{
		y_given_lambda += Gamma.Logp(m.N[i],m.N[i]*lambda[i],m.Y[i])
	}

	ll := prior_log_beta + prior_lambda + y_given_lambda
	return ll
}



