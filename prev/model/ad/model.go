package model

import (
	. "bitbucket.org/dtolpin/infergo/dist/ad"
	"bitbucket.org/dtolpin/infergo/ad"
	"math"
)

type Model struct {
	J	int
	Y	[]float64
	N	[]float64
}

func (m *Model) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var beta float64
	ad.Assignment(&beta, ad.Value(10.0))
	var alpha float64
	ad.Assignment(&alpha, ad.Elemental(math.Exp, &x[0]))
	var lambda []float64

	lambda = make([]float64, len(x)-1)
	for i := range lambda {
		ad.Assignment(&lambda[i], ad.Elemental(math.Exp, &x[i+1]))
	}
	var prior_log_alpha float64
	ad.Assignment(&prior_log_alpha, ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, ad.Value(1), ad.Value(5), &x[0]))
	var prior_lambda float64
	ad.Assignment(&prior_lambda, ad.Call(func(_ []float64) {
		Gamma.Logps(0, 0, lambda...)
	}, 2, &alpha, &beta))
	var y_given_lambda float64
	ad.Assignment(&y_given_lambda, ad.Value(0.0))
	for i := range lambda {
		ad.Assignment(&y_given_lambda, ad.Arithmetic(ad.OpAdd, &y_given_lambda, ad.Call(func(_ []float64) {
			Gamma.Logp(0, 0, 0)
		}, 3, &m.N[i], ad.Arithmetic(ad.OpMul, &m.N[i], &lambda[i]), &m.Y[i])))
	}
	var ll float64
	ad.Assignment(&ll, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpAdd, &prior_log_alpha, &prior_lambda), &y_given_lambda))
	return ad.Return(&ll)
}
